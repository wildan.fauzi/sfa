package com.sfa.id.master.provinsi.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import com.sfa.id.master.kota.Kota;
import com.sfa.id.master.provinsi.Provinsi;
import com.sfa.id.master.provinsi.ProvinsiRepository;
import com.sfa.id.master.provinsi.dto.ProvinsiResponseDto;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class ProvinsiQueryService {

	private final ProvinsiRepository repository;
	
	private final ModelMapper mapper;
	
	public List<Provinsi> getAll() {
		return repository.findAll();
	}
	
	public List<Kota> findKotaByProvinsiId(Long id) {
		
		Optional<Provinsi> listProvinsi = repository.findById(id);
		List<Kota> listKota = new ArrayList<>(listProvinsi.get().getKota());
		
		return listKota;
	}

	public ProvinsiResponseDto findById(Long id) {
		Provinsi provinsi = repository.findById(id).orElse(null);
		
		
		if (provinsi == null) {
			return null;
		}
		return mapper.map(provinsi, ProvinsiResponseDto.class);
	}
	
	
}
