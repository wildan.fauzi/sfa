package com.sfa.id.master.provinsi;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sfa.id.core._enum.ResponseCodeEnum;
import com.sfa.id.core.response.BaseResponse;
import com.sfa.id.master.kota.Kota;
import com.sfa.id.master.provinsi.dto.ProvinsiResponseDto;
import com.sfa.id.master.provinsi.service.ProvinsiQueryService;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/provinsi")
@RequiredArgsConstructor
public class ProvinsiController {

	private final ProvinsiQueryService service;
	
	private final ModelMapper mapper;
	
	@GetMapping
	public ResponseEntity<?> getAll() {
		BaseResponse<List<ProvinsiResponseDto>> response = new BaseResponse<>();
		List<Provinsi> listProvinsi = service.getAll();
		
		List<ProvinsiResponseDto> data = listProvinsi
											.stream().map(provinsi -> mapper.map(provinsi, ProvinsiResponseDto.class))
											.collect(Collectors.toList());
		
		response.setStatusCode(ResponseCodeEnum.VIEW_SUCCESS);
		response.setData(data);
		
		return ResponseEntity.ok(response);
	}
	
	@GetMapping("/{provinsi_id}/kota")
	public ResponseEntity<?> findKotaByProvinsiId(@PathVariable("provinsi_id") Long id) {
		BaseResponse<List<Kota>> response = new BaseResponse<>();
		
		response.setStatusCode(ResponseCodeEnum.VIEW_SUCCESS);
		response.setData(service.findKotaByProvinsiId(id));
		
		return ResponseEntity.ok(response);
	}
	
	@GetMapping("/{provinsi_id}")
	public ResponseEntity<?> findById(@PathVariable("provinsi_id") Long id) {
		BaseResponse<ProvinsiResponseDto> response = new BaseResponse<>();

		
		response.setStatusCode(ResponseCodeEnum.VIEW_SUCCESS);
		response.setData(service.findById(id));
		
		return ResponseEntity.ok(response);
	}
	
}
