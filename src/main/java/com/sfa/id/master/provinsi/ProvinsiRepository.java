package com.sfa.id.master.provinsi;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sfa.id.core.base.BaseRepository;
import com.sfa.id.master.kota.Kota;

@Repository
public interface ProvinsiRepository extends JpaRepository<Provinsi, Long>{
	
	
	List<Kota> findKotaById(Long id);
}
