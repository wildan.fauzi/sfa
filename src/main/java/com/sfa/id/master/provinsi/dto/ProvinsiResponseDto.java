package com.sfa.id.master.provinsi.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ProvinsiResponseDto {

	private Long id;
	private String nama;
}
