package com.sfa.id.master.produk;

import java.util.Arrays;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import com.sfa.id.core.base.BaseSpecification;

@Component
public class ProdukSpecification extends BaseSpecification<Produk> {

	@Override
	public Specification<Produk> containsTextInOmni(String text) {
		return containsTextInAttributes(text, Arrays.asList("namaProduk", "deskripsi", "sku"));
	}

	
	public Specification<Produk> filterByKategori(Long kategoriId, String status, String search ) {
		search = (search != null) ? search : "";
        status = (status != null) ? status.toLowerCase() : "";
        switch (status) {
            case "isdeleted":
                return containsTextInOmni(search).and(this.isDeleted().and(isKategori(kategoriId)));
            case "notdeleted":
                return containsTextInOmni(search).and(this.notDeleted().and(isKategori(kategoriId)));
            default: //ambil semua yg delete ataupun tidak
                return containsTextInOmni(search);
        }
	}
	
	public Specification<Produk> filterBySatuan(String satuan, String status, String search) {
		search = (search != null) ? search : "";
        status = (status != null) ? status.toLowerCase() : "";
        switch (status) {
            case "isdeleted":
                return containsTextInOmni(search).and(this.isDeleted().and(isSatuan(satuan)));
            case "notdeleted":
                return containsTextInOmni(search).and(this.notDeleted().and(isSatuan(satuan)));
            default: //ambil semua yg delete ataupun tidak
                return containsTextInOmni(search);
        }
	}
	
	public Specification<Produk> filterByKategoriAndSatuan(Long kategoriId, String satuan, String status, String search ) {
		search = (search != null) ? search : "";
        status = (status != null) ? status.toLowerCase() : "";
        switch (status) {
            case "isdeleted":
                return containsTextInOmni(search).and(this.isDeleted()
                		.and(isKategori(kategoriId))
                		.and(isSatuan(satuan))
                		);
            case "notdeleted":
                return containsTextInOmni(search).and(this.notDeleted()
                		.and(isKategori(kategoriId))
                		.and(isSatuan(satuan))
                		);
            default: //ambil semua yg delete ataupun tidak
                return containsTextInOmni(search);
        }
	}
	
	private Specification<Produk> isKategori(Long kategoriId) {
		return (root, query, builder) -> builder.equal(root.get("kategori"), kategoriId);
	}
	
	private Specification<Produk> isSatuan(String satuan) {
		return (root, query, builder) -> builder.equal(root.get("satuan"), satuan.toLowerCase());
	}
}
