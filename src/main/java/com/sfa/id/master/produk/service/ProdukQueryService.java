package com.sfa.id.master.produk.service;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.sfa.id.core.base.BasePageInterface;
import com.sfa.id.core.base.QueryServiceInterface;
import com.sfa.id.master.produk.Produk;
import com.sfa.id.master.produk.ProdukRepository;
import com.sfa.id.master.produk.ProdukSpecification;
import com.sfa.id.master.produk.dto.ProdukRequestDto;
import com.sfa.id.master.produk.dto.ProdukResponseDto;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class ProdukQueryService implements QueryServiceInterface<ProdukRequestDto, Produk>,
													BasePageInterface<Produk, ProdukSpecification, ProdukRequestDto, Long>
	{

	private final ProdukRepository produkRepository;
	
	private final ModelMapper mapper;
	
	private final ProdukSpecification spec;
	
	public Page<ProdukResponseDto> getAll(
			String search, 
			Integer page, 
			Integer limit, 
			List<String> sortBy, 
			Boolean desc,
			Boolean isDeleted,
			Long kategoriId,
			String satuan
			) {
		
		String defaultSort = "id|asc";
		if (sortBy != null) {
			sortBy.add(defaultSort);
		} else {
			sortBy = Arrays.asList(defaultSort, "id|asc");
		}
		
		isDeleted = (isDeleted != null) ? isDeleted : false;
		
		Pageable pageableRequest = this.defaultPage(search, page, limit, sortBy, desc);
		String status = "notdeleted";
		if (isDeleted == true) {
			status = "isdeleted";
		}
		
		Specification<Produk> filterSpec;
		if (kategoriId != null && satuan != null) {
			filterSpec = spec.filterByKategoriAndSatuan(kategoriId, satuan, status, search);
		} else if (kategoriId != null) {
			filterSpec = spec.filterByKategori(kategoriId, status, search);
		} else if (satuan != null) {
			filterSpec = spec.filterBySatuan(satuan, status, search);
		} else {
			 filterSpec= this.defaultSpec(search, status, spec);
		}
		
		Page<Produk> produkPage = produkRepository.findAll(filterSpec, pageableRequest);
		List<Produk> produkList = produkPage.getContent();
		List<ProdukResponseDto> responseDto = produkList.stream().map(produk -> mapper.map(produk, ProdukResponseDto.class))
				.collect(Collectors.toList());
		return new PageImpl<>(responseDto, pageableRequest, produkPage.getTotalElements());
	}
}
