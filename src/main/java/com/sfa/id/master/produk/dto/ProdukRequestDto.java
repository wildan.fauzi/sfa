package com.sfa.id.master.produk.dto;

import java.math.BigDecimal;

import javax.validation.constraints.NotBlank;

import com.sfa.id.master.kategori.Kategori;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class ProdukRequestDto {
	
	private Long id;
	
	@NotBlank
	private Long kategoriId;
	
	@NotBlank
	private String sku;
	
	@NotBlank
	private String namaProduk;
	
	@NotBlank
	private String satuan;
	
	@NotBlank
	private String deskripsi;
	
	@NotBlank
	private BigDecimal harga;
}
