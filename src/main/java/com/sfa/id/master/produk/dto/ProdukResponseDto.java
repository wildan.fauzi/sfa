package com.sfa.id.master.produk.dto;

import java.math.BigDecimal;

import com.sfa.id.master.kategori.dto.KategoriResponseDto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ProdukResponseDto {
	private Long id;
	private String sku;
	private String namaProduk;
	private String satuan;
	private String deskripsi;
	private BigDecimal harga;
	private KategoriResponseDto kategori;
	
}
