package com.sfa.id.master.produk;

import org.springframework.stereotype.Repository;

import com.sfa.id.core.base.BaseRepository;

@Repository
public interface ProdukRepository extends BaseRepository<Produk, Long>{

}
