package com.sfa.id.master.produk;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.sfa.id.core.base.Base;
import com.sfa.id.master.kategori.Kategori;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "m_produk")
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Produk extends Base{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "produk_id")
	private Long id;
	
	@Column(name = "sku", nullable = false)
	private String sku;
	
	@Column(name = "nama_produk", nullable = false)
	private String namaProduk;
	
	@Column(name = "satuan", nullable = false)
	private String satuan;
	
	@Column(name = "deskripsi", nullable = false)
	private String deskripsi;
	
	@Column(name = "harga", nullable = false)
	private BigDecimal harga;
	
	@ManyToOne
	@JoinColumn(name = "kategori_id")
	private Kategori kategori;
}
