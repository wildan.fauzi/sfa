package com.sfa.id.master.produk.service;

import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import com.sfa.id.core._enum.SatuanEnum;
import com.sfa.id.core.base.TransactionServiceInterface;
import com.sfa.id.core.response.BaseError;
import com.sfa.id.core.response.BaseResponse;
import com.sfa.id.master.kategori.Kategori;
import com.sfa.id.master.kategori.KategoriRepository;
import com.sfa.id.master.produk.Produk;
import com.sfa.id.master.produk.ProdukRepository;
import com.sfa.id.master.produk.dto.ProdukRequestDto;
import com.sfa.id.master.produk.dto.ProdukResponseDto;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
@SuppressWarnings("unchecked")
public class ProdukTransactionService implements TransactionServiceInterface<ProdukRequestDto, Produk>{
	
	private final ProdukRepository produkRepository;
	
	private final ModelMapper mapper;
	
	private final KategoriRepository kategoriRepository;
	
	@Override
	public boolean beforeSave(BaseResponse baseResponse, ProdukRequestDto request) {
		return checkKategori(baseResponse, request) && checkSatuan(baseResponse, request);
	}
	
	@Override
	public boolean save(BaseResponse baseResponse, ProdukRequestDto request) {
		Produk produk = requestToProduk(request);
		ProdukResponseDto data = mapper.map(produk, ProdukResponseDto.class);
		
		try {
			produkRepository.save(produk);
			baseResponse.setData(data);
			return true;
		} catch (Exception e) {
			baseResponse.getErrorMessages()
            .add(BaseError.builder()
                    .field("Produk")
                    .message("Gagal menambahkan Produk")
                    .build());
			return false;
		}
		
	}
	
	@Override
	public boolean beforeUpdate(BaseResponse baseResponse, ProdukRequestDto request) {
		
		return checkExist(baseResponse, request) && checkKategori(baseResponse, request) && checkSatuan(baseResponse, request);
	}
	
	@Override
	public boolean update(BaseResponse baseResponse, ProdukRequestDto request) {
		Produk produk = requestToProduk(request);
		ProdukResponseDto data = mapper.map(produk, ProdukResponseDto.class);
		
		try {
			produkRepository.save(produk);
			baseResponse.setData(data);
			return true;
		} catch (Exception e) {
			baseResponse.getErrorMessages()
            .add(BaseError.builder()
                    .field("Produk")
                    .message("Gagal update Produk")
                    .build());
			return false;
		}
		
	}
	
	
	private boolean checkExist(BaseResponse response, ProdukRequestDto request) {
		Produk produk = produkRepository.findById(request.getId()).orElse(null);
		
		if (produk == null) {
			response.getErrorMessages().add(
					BaseError.builder()
					.field("Produk")
					.message("Tidak dapat menemukan Produk dengan id: "+ request.getId())
					);
			return false;
		}
		
		if (produk.getDeletionFlag()) {
			response.getErrorMessages().add(
					BaseError.builder()
					.field("Produk")
					.message("Produk telah dihapus. id: "+ request.getId())
					);
			return false;
		}
		
		
		return true;
	}
	
	private boolean checkKategori(BaseResponse response, ProdukRequestDto request) {		
		Kategori kategori = kategoriRepository.findById(request.getKategoriId()).orElse(null);
		
		if (kategori == null) {
			response.getErrorMessages()
            .add(BaseError.builder()
                    .field("Kategori")
                    .message("Gagal menambahkan Produk. Kategori tidak terdaftar")
                    .build());
			return false;
		}
		
		return true;
	}
	
	private boolean checkSatuan(BaseResponse baseResponse, ProdukRequestDto request) {
		
		String satuan = request.getSatuan().toUpperCase();
		
		try {
			request.setSatuan(SatuanEnum.valueOf(satuan).getName());
			return true;
		} catch (Exception e) {
			baseResponse.getErrorMessages()
				.add(BaseError.builder()
                  .field("Satuan")
                  .message("Gagal menambahkan Produk. Satuan tidak terdaftar")
                  .build());
		}
		
		return false;
	}
	
	private Produk requestToProduk(ProdukRequestDto request) {
		Produk produk = mapper.map(request, Produk.class);
		Kategori kategori = kategoriRepository.getById(request.getKategoriId());
		produk.setKategori(kategori);
		produk.setDeletionFlag(false);
		return produk;
	}
}
