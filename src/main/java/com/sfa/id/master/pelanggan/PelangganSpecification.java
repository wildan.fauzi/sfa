package com.sfa.id.master.pelanggan;

import java.util.Arrays;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import com.sfa.id.core.base.BaseSpecification;

@Component
public class PelangganSpecification extends BaseSpecification<Pelanggan>{

	@Override
	public Specification<Pelanggan> containsTextInOmni(String text) {
		return containsTextInAttributes(text, Arrays.asList("namaToko", "email", "alamat"));
	}

}
