package com.sfa.id.master.pelanggan;

import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sfa.id.core._enum.ResponseCodeEnum;
import com.sfa.id.core.response.BaseResponse;
import com.sfa.id.master.pelanggan.dto.PelangganRequestDto;
import com.sfa.id.master.pelanggan.dto.PelangganResponseDto;
import com.sfa.id.master.pelanggan.service.PelangganQueryService;
import com.sfa.id.master.pelanggan.service.PelangganTransactionService;
import com.sfa.id.master.produk.dto.ProdukRequestDto;
import com.sfa.id.master.produk.dto.ProdukResponseDto;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/pelanggan")
@RequiredArgsConstructor
public class PelangganController {

	private final PelangganTransactionService transactionService;
	
	private final PelangganQueryService queryService;
	
	private final ModelMapper mapper;
	
	@GetMapping
	public ResponseEntity<?> getAll(
			@RequestParam(value = "search", required = false) String search,
			@RequestParam(value = "page", required = false) Integer page,
			@RequestParam(value = "limit", required = false) Integer limit,
			@RequestParam(value = "sortBy", required = false) List<String> sortBy,
			@RequestParam(value = "descending", required = false) Boolean desc,
			@RequestParam(value = "isDeleted", required = false) Boolean isDeleted
			) {
		BaseResponse<Page<PelangganResponseDto>> response = new BaseResponse<>();
		Page<PelangganResponseDto> data = queryService.getAll(search, page, limit, sortBy, desc, isDeleted);
		
		response.setErrorMessages(null);
		response.setStatusCode(ResponseCodeEnum.SUCCESS);
		response.setData(data);
		
		return ResponseEntity.ok(response);
	}
	
	@PostMapping
	public ResponseEntity<?> store(@Valid @RequestBody PelangganRequestDto requestDto) {
		BaseResponse<PelangganResponseDto> response = new BaseResponse<>();
		
		if (!transactionService.doSave(response, requestDto)) {
			response.setData(null);
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
		}
		
		PelangganResponseDto responseDto = mapper.map(requestDto, PelangganResponseDto.class);
		
		response.setStatusCode(ResponseCodeEnum.SAVE_SUCCESS.getCode());
		response.setMessage(ResponseCodeEnum.SAVE_SUCCESS.getDescription());
		response.setErrorMessages(null);
		response.setData(responseDto);
		
		return ResponseEntity.ok(response);
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<?> update(@Valid @RequestBody PelangganRequestDto request, @PathVariable("id") Long id) {
		BaseResponse<PelangganResponseDto> response = new BaseResponse<>();
		request.setId(id);
		
		if (!transactionService.doUpdate(response, request)) {
			response.setData(null);
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
		}
		
		PelangganResponseDto responseDto = mapper.map(request, PelangganResponseDto.class);
		
		response.setStatusCode(ResponseCodeEnum.UPDATE_SUCCESS.getCode());
		response.setMessage(ResponseCodeEnum.UPDATE_SUCCESS.getDescription());
		response.setErrorMessages(null);
		response.setData(responseDto);
		
		
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}
}
