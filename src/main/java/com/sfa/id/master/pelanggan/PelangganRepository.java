package com.sfa.id.master.pelanggan;

import org.springframework.stereotype.Repository;

import com.sfa.id.core.base.BaseRepository;

@Repository
public interface PelangganRepository extends BaseRepository<Pelanggan, Long>{

}
