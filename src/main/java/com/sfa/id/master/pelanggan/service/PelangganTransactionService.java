package com.sfa.id.master.pelanggan.service;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import com.sfa.id.auth.user.User;
import com.sfa.id.auth.user.UserRepository;
import com.sfa.id.core._enum.KategoriPajakEnum;
import com.sfa.id.core._enum.ReferensiHargaEnum;
import com.sfa.id.core._enum.StatusTokoEnum;
import com.sfa.id.core.base.TransactionServiceInterface;
import com.sfa.id.core.response.BaseError;
import com.sfa.id.core.response.BaseResponse;
import com.sfa.id.master.kota.Kota;
import com.sfa.id.master.kota.KotaRepository;
import com.sfa.id.master.pelanggan.Pelanggan;
import com.sfa.id.master.pelanggan.PelangganRepository;
import com.sfa.id.master.pelanggan.dto.PelangganRequestDto;
import com.sfa.id.master.produk.Produk;
import com.sfa.id.master.produk.dto.ProdukRequestDto;
import com.sfa.id.master.provinsi.Provinsi;
import com.sfa.id.master.provinsi.ProvinsiRepository;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
@SuppressWarnings("unchecked")
public class PelangganTransactionService implements TransactionServiceInterface<PelangganRequestDto, Pelanggan> {

	private final PelangganRepository pelangganRepository;
	
	private final ProvinsiRepository provinsiRepository;
	
	private final KotaRepository kotaRepository;
	
	private final UserRepository userRepository;
	
	private final ModelMapper mapper;
	
	@Override
	public boolean beforeSave(BaseResponse baseResponse, PelangganRequestDto request) {
		return checkStatus(baseResponse, request) && 
				checkProvinsi(baseResponse, request) &&
				checkKota(baseResponse, request) &&
				checkSalesManager(baseResponse, request) &&
				checkSalesPerson(baseResponse, request) &&
				checkReferensiHarga(baseResponse, request)	
				;
	}
	
	@Override
	public boolean save(BaseResponse baseResponse, PelangganRequestDto request) {
		Pelanggan pelanggan = requestToPelanggan(request);
		pelanggan.setDeletionFlag(false);
			
		try {
			pelangganRepository.save(pelanggan);	
			return true;
		} catch (Exception e) {
			baseResponse.getErrorMessages()
            .add(BaseError.builder()
                    .field("Pelanggan")
                    .message("Gagal menambahkan Pelanggan")
                    .build());
			return false;
		}
		
	}
	
	@Override
	public boolean beforeUpdate(BaseResponse baseResponse, PelangganRequestDto request) {
		return checkExist(baseResponse, request) && checkStatus(baseResponse, request);
	}
	
	@Override
	public boolean update(BaseResponse baseResponse, PelangganRequestDto request) {
		Pelanggan pelanggan = requestToPelanggan(request);
		pelanggan.setDeletionFlag(false);
		
		try {
			pelangganRepository.save(pelanggan);
			return true;
		} catch (Exception e) {
			baseResponse.getErrorMessages()
            .add(BaseError.builder()
                    .field("Pelanggan")
                    .message("Gagal update Pelanggan")
                    .build());
			return false;
		}
	}
	
	
	private boolean checkExist(BaseResponse response, PelangganRequestDto request) {
		Pelanggan pelanggan = pelangganRepository.findById(request.getId()).orElse(null);
		
		if (pelanggan == null) {
			response.getErrorMessages().add(
					BaseError.builder()
					.field("Pelanggan")
					.message("Tidak dapat menemukan Pelanggan dengan id: "+ request.getId())
					);
			return false;
		}
		
		if (pelanggan.getDeletionFlag()) {
			response.getErrorMessages().add(
					BaseError.builder()
					.field("Produk")
					.message("Pelanggan telah dihapus. id: "+ request.getId())
					);
			return false;
		}
		
		
		return true;
	}
	
	private boolean checkStatus(BaseResponse response, PelangganRequestDto request) {
		
		String status = request.getStatus().toUpperCase();
		
		try {
			request.setStatus(StatusTokoEnum.valueOf(status).getName());
			return true;
		} catch (Exception e) {
			response.getErrorMessages()
            .add(BaseError.builder()
                    .field("Status")
                    .message("Gagal menambahkan Pelanggan. Status tidak terdaftar (Normal/Peringatan)")
                    .build());
		}
		
		return false;
	}
	
	private boolean checkProvinsi(BaseResponse response, PelangganRequestDto request) {
		Provinsi provinsi = provinsiRepository.findById(request.getProvinsiId()).orElse(null);
		
		if (provinsi == null) {
			response.getErrorMessages().add(
					BaseError.builder()
					.field("Provinsi")
					.message("Tidak dapat menemukan Provinsi dengan id: "+ request.getProvinsiId())
					);
			return false;
		}
		
		return true;
	}
	
	private boolean checkKota(BaseResponse response, PelangganRequestDto request) {
		Kota kota= kotaRepository.findById(request.getKotaId()).orElse(null);
		
		if (kota == null) {
			response.getErrorMessages().add(
					BaseError.builder()
					.field("Kota")
					.message("Tidak dapat menemukan Kota dengan id: "+ request.getKotaId())
					);
			return false;
		}
		
		return true;
	}
	
	private boolean checkSalesManager(BaseResponse response, PelangganRequestDto request) {
		User user = userRepository.findById(request.getSalesManagerId()).orElse(null);
		boolean isExist = true;
		boolean isSalesManager = true;
		
		
		if (user == null) {
			response.getErrorMessages().add(
					BaseError.builder()
					.field("User")
					.message("Tidak dapat menemukan User dengan id: "+ request.getSalesManagerId())
					);
			isExist = false;
		}
		
		if(!user.getRole().getName().equalsIgnoreCase("Sales Manager")) {
			response.getErrorMessages().add(
					BaseError.builder()
					.field("User")
					.message("User: " + user.getNamaDepan() + " " + user.getNamaBelakang() + " bukan Sales Manager")
					);
			isSalesManager = false;
		}
		
		return isExist && isSalesManager;
	}
	
	private boolean checkSalesPerson(BaseResponse response, PelangganRequestDto request) {
		User user = userRepository.findById(request.getSalesPersonId()).orElse(null);
		boolean isExist = true;
		boolean isSalesPerson = true;
		
		
		if (user == null) {
			response.getErrorMessages().add(
					BaseError.builder()
					.field("User")
					.message("Tidak dapat menemukan User dengan id: "+ request.getSalesManagerId())
					);
			isExist = false;
		}
		
		if(!user.getRole().getName().equalsIgnoreCase("Sales Person")) {
			response.getErrorMessages().add(
					BaseError.builder()
					.field("User")
					.message("User: " + user.getNamaDepan() + " " + user.getNamaBelakang() + " bukan Sales Person")
					);
			isSalesPerson = false;
		}
		
		return isExist && isSalesPerson;
	}
	
	private boolean checkKategoriPajak(BaseResponse response, PelangganRequestDto request) {
		String kategoriPajak = request.getKategoriPajak().toUpperCase();
		
		try {
			request.setKategoriPajak(KategoriPajakEnum.valueOf(kategoriPajak).getName());
			return true;
		} catch (Exception e) {
			response.getErrorMessages()
			.add(BaseError.builder()
              .field("Kategori Pajak")
              .message("Gagal menambahkan Pelanggan. Kategori Pajak tidak terdaftar. (PKP / NONPKP)")
              .build());
		}
		
		return false;
	}
	
	private boolean checkReferensiHarga(BaseResponse response, PelangganRequestDto request) {
		String referensiHarga = request.getReferensiHarga().toUpperCase();
		try {
			request.setReferensiHarga(ReferensiHargaEnum.valueOf(referensiHarga).getName());
			return true;
		} catch (Exception e) {
			response.getErrorMessages()
			.add(BaseError.builder()
              .field("Referensi Harga")
              .message("Gagal menambahkan Pelanggan. Referensi Harga tidak terdaftar. (LUARKOTA/DALAMKOTA)")
              .build());
		}
		
		return false;
	}
	
	private Pelanggan requestToPelanggan(PelangganRequestDto request) {
		Pelanggan pelanggan = mapper.map(request, Pelanggan.class);
		Provinsi provinsi = provinsiRepository.findById(request.getProvinsiId()).orElse(null);
		Kota kota = kotaRepository.findById(request.getKotaId()).orElse(null);
		User salesManager = userRepository.findById(request.getSalesManagerId()).orElse(null);
		User salesPerson = userRepository.findById(request.getSalesPersonId()).orElse(null);
		
		pelanggan.setProvinsi(provinsi);
		pelanggan.setKota(kota);
		pelanggan.setSalesManager(salesManager);
		pelanggan.setSalesPerson(salesPerson);
		
		return pelanggan;
		
	}
}
