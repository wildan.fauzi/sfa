package com.sfa.id.master.pelanggan.dto;

import java.math.BigDecimal;

import com.sfa.id.auth.user.dto.UserSummaryResponseDto;
import com.sfa.id.master.kota.Kota;
import com.sfa.id.master.provinsi.dto.ProvinsiResponseDto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class PelangganResponseDto {

	private Long id;
	private String namaToko;
	private String namaPemilik;
	private String noTelepon;
	private String email;
	private String status;
	private ProvinsiResponseDto provinsi;
	private Kota kota;
	private String alamat;
	private String nik;
	private String imageNik;
	private String npwp;
	private String imageNpwp;
	private String kategoriPajak;
	private String pic;
	private String noTeleponPic;
	private UserSummaryResponseDto salesManager;
	private UserSummaryResponseDto salesPerson;
	private String referensiHarga;
	private BigDecimal diskonReguler;
	private BigDecimal diskonDistributor;
}
