package com.sfa.id.master.pelanggan.service;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.sfa.id.core.base.BasePageInterface;
import com.sfa.id.core.base.QueryServiceInterface;
import com.sfa.id.master.pelanggan.Pelanggan;
import com.sfa.id.master.pelanggan.PelangganRepository;
import com.sfa.id.master.pelanggan.PelangganSpecification;
import com.sfa.id.master.pelanggan.dto.PelangganRequestDto;
import com.sfa.id.master.pelanggan.dto.PelangganResponseDto;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class PelangganQueryService implements QueryServiceInterface<PelangganRequestDto, Pelanggan>,
										BasePageInterface<Pelanggan, PelangganSpecification, PelangganRequestDto, Long>{
	
	private final PelangganRepository pelangganRepository;
	
	private final ModelMapper mapper; 
	
	private final PelangganSpecification spec;
	
	public Page<PelangganResponseDto> getAll(
			String search, 
			Integer page, 
			Integer limit, 
			List<String> sortBy, 
			Boolean desc,
			Boolean isDeleted) {
		
		String defaultSort = "id|asc";
		if (sortBy != null) {
			sortBy.add(defaultSort);
		} else {
			sortBy = Arrays.asList(defaultSort, "id|asc");
		}
		
		isDeleted = (isDeleted != null) ? isDeleted : false;
		
		Pageable pageableRequest = this.defaultPage(search, page, limit, sortBy, desc);
		String status = "notdeleted";
		if (isDeleted == true) {
			status = "isdeleted";
		}
		
		Specification<Pelanggan> filterSpec = this.defaultSpec(search, status, spec);
		
		Page<Pelanggan> pelangganPage = pelangganRepository.findAll(filterSpec, pageableRequest);
		List<Pelanggan> pelangganList = pelangganPage.getContent();
		List<PelangganResponseDto> responseDto = pelangganList.stream().map(produk -> mapper.map(produk, PelangganResponseDto.class))
				.collect(Collectors.toList());
		return new PageImpl<>(responseDto, pageableRequest, pelangganPage.getTotalElements());
	}

}
