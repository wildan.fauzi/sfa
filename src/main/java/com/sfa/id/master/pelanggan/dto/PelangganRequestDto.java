package com.sfa.id.master.pelanggan.dto;

import java.math.BigDecimal;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

import com.sfa.id.core.regex.RegexStringPattern;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class PelangganRequestDto {
	
	private Long id;
	
	@NotBlank
	private String namaToko;
	
	@NotBlank
	private String namaPemilik;
	
	@NotBlank
	private String noTelepon;
	
	@Email
	@NotBlank
	@Pattern(regexp = RegexStringPattern.EMAIL_FORMAT)
	private String email;
	
	@NotBlank
	private String status;
	
	@NotBlank
	private Long provinsiId;
	
	@NotBlank
	private Long kotaId;
	
	@NotBlank
	private String alamat;
	
	@NotBlank
	private String nik;
	
	private String imageNik;

	@NotBlank
	private String npwp;
	
	private String imageNpwp;

	@NotBlank
	private String kategoriPajak;

	@NotBlank
	private String pic;

	@NotBlank
	private String noTeleponPic;

	@NotBlank
	private Long salesManagerId;

	@NotBlank
	private Long salesPersonId;

	@NotBlank
	private String referensiHarga;

	private BigDecimal diskonReguler;

	private BigDecimal diskonDistributor;
}
