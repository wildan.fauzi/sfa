package com.sfa.id.master.pelanggan;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.sfa.id.auth.user.User;
import com.sfa.id.core._enum.StatusTokoEnum;
import com.sfa.id.core.base.Base;
import com.sfa.id.master.kota.Kota;
import com.sfa.id.master.produk.Produk;
import com.sfa.id.master.provinsi.Provinsi;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "m_pelanggan")
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Pelanggan extends Base{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "nama_toko", nullable = false)
	private String namaToko;
	
	@Column(name = "nama_pemilik", nullable = false)
	private String namaPemilik;
	
	@Column(name = "no_telepon", nullable = false)
	private String noTelepon;
	
	@Column(name = "email", nullable = false)
	private String email;
	
	@Column(name = "status", nullable = false)
	private String status;

	@Column(name = "deskripsi")
	private String deskripsi;
	
	@ManyToOne
	@JoinColumn(name = "provinsi_id")
	private Provinsi provinsi;
	
	@ManyToOne
	@JoinColumn(name = "kota_id")
	private Kota kota;
	
	@Column(name = "alamat", nullable = false)
	private String alamat;
	
	@Column(name = "nik", nullable = false)
	private String nik;
	
	@Column(name = "image_nik")
	private String imageNik;
	
	@Column(name = "npwp", nullable = false)
	private String npwp;
	
	@Column(name = "image_npwp")
	private String imageNpwp;
	
	@Column(name = "kategori_pajak", nullable = false)
	private String kategoriPajak;
	
	@Column(name = "pic", nullable = false)
	private String pic;
	
	@Column(name = "no_telepon_pic", nullable = false)
	private String noTeleponPic;
	
	@ManyToOne
	@JoinColumn(name = "sales_manager_id")
	private User salesManager;
	
	@ManyToOne
	@JoinColumn(name = "sales_person_id")
	private User salesPerson;
	
	@Column(name = "referensi_harga")
	private String referensiHarga;
	
	@Column(name = "diskon_reguler")
	private BigDecimal diskonReguler;
	
	@Column(name = "diskon_distributor")
	private BigDecimal diskonDistributor;	
	
}
