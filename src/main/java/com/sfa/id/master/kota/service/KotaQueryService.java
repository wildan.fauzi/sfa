package com.sfa.id.master.kota.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.sfa.id.master.kota.Kota;
import com.sfa.id.master.kota.KotaRepository;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class KotaQueryService {

	private final KotaRepository repository;
	
	public List<Kota> getAll() {
		return repository.findAll();
	}
	
	public List<Kota> findByProvinsiId(Long id) {
		return repository.findByProvinsiId(id);
	}
	
	public Kota findById(Long id) {
		return repository.findById(id).orElse(null);
	}
}
