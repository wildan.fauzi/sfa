package com.sfa.id.master.kota;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface KotaRepository extends JpaRepository<Kota, Long>{

	List<Kota> findByProvinsiId(Long id);
}
