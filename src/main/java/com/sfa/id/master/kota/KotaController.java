package com.sfa.id.master.kota;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sfa.id.core._enum.ResponseCodeEnum;
import com.sfa.id.core.response.BaseResponse;
import com.sfa.id.master.kota.service.KotaQueryService;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/kota")
@RequiredArgsConstructor
public class KotaController {

	private final KotaQueryService queryService;
	
	@GetMapping("/{kota_id}")
	public ResponseEntity<?> findById(@PathVariable("kota_id") Long id) {
		BaseResponse<Kota> response = new BaseResponse<>();
		
		response.setStatusCode(ResponseCodeEnum.VIEW_SUCCESS);
		response.setData(queryService.findById(id));
		
		return ResponseEntity.ok(response);
	}
	
	@GetMapping()
	public ResponseEntity<?> findAll() {
		BaseResponse<List<Kota>> response = new BaseResponse<>();
		
		response.setStatusCode(ResponseCodeEnum.VIEW_SUCCESS);
		response.setData(queryService.getAll());
		
		return ResponseEntity.ok(response);
	}
}
