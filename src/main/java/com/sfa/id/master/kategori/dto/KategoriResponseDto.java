package com.sfa.id.master.kategori.dto;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class KategoriResponseDto {
	private Long id;
	private String namaKategori;
}
