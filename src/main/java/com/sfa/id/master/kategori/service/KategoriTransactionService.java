package com.sfa.id.master.kategori.service;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import com.sfa.id.core.base.TransactionServiceInterface;
import com.sfa.id.core.response.BaseError;
import com.sfa.id.core.response.BaseResponse;
import com.sfa.id.master.kategori.Kategori;
import com.sfa.id.master.kategori.KategoriRepository;
import com.sfa.id.master.kategori.dto.KategoriRequestDto;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class KategoriTransactionService implements TransactionServiceInterface<KategoriRequestDto, Kategori> {

	
	private final KategoriRepository kategoriRepository;
	
	
	private final ModelMapper mapper;
	
	@Override
	public boolean save(BaseResponse baseResponse, KategoriRequestDto request) {
		Kategori kategori = mapper.map(request, Kategori.class);
		kategori.setDeletionFlag(false);
		
		try {
			kategoriRepository.save(kategori);
			return true;
		} catch (Exception e) {
			baseResponse.getErrorMessages()
            .add(BaseError.builder()
                    .field("Kategori")
                    .message("Gagal menambahkan kategori")
                    .build());
			return false;
		}
	}
	
	@Override
	public boolean beforeUpdate(BaseResponse baseResponse, KategoriRequestDto request) {
		return this.checkExist(baseResponse, request);
	}
	
	@Override
	public boolean update(BaseResponse baseResponse, KategoriRequestDto request) {
		Kategori kategori = mapper.map(request, Kategori.class);
		kategori.setDeletionFlag(false);
		
		try {
			kategoriRepository.save(kategori);
			return true;
		} catch (Exception e) {
			baseResponse.getErrorMessages()
            .add(BaseError.builder()
                    .field("Kategori")
                    .message("Gagal update kategori")
                    .build());
			return false;
		}
	}
	
	
	
	
	
	
	private boolean checkExist(BaseResponse response, KategoriRequestDto request) {
		Kategori kategori = kategoriRepository.findById(request.getId()).orElse(null);
		
		if (kategori == null) {
			response.getErrorMessages().add(
					BaseError.builder()
					.field("Kategori")
					.message("Tidak dapat menemukan kategori dengan id: "+ request.getId())
					);
			return false;
		}
		
		if (kategori.getDeletionFlag()) {
			response.getErrorMessages().add(
					BaseError.builder()
					.field("Kategori")
					.message("Kategori telah dihapus. id: "+ request.getId())
					);
			return false;
		}
		
		
		return true;
	}
}
