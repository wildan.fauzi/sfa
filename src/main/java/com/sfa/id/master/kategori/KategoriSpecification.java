package com.sfa.id.master.kategori;

import java.util.Arrays;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import com.sfa.id.core.base.BaseSpecification;

@Component
public class KategoriSpecification extends BaseSpecification<Kategori>{

	@Override
	public Specification<Kategori> containsTextInOmni(String text) {
		return containsTextInAttributes(text, Arrays.asList("namaKategori"));
	}

}
