package com.sfa.id.master.kategori.service;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.sfa.id.core.base.BasePageInterface;
import com.sfa.id.core.base.QueryServiceInterface;
import com.sfa.id.master.kategori.Kategori;
import com.sfa.id.master.kategori.KategoriRepository;
import com.sfa.id.master.kategori.KategoriSpecification;
import com.sfa.id.master.kategori.dto.KategoriRequestDto;
import com.sfa.id.master.kategori.dto.KategoriResponseDto;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class KategoriQueryService implements QueryServiceInterface<KategoriRequestDto, Kategori>,
BasePageInterface<Kategori, KategoriSpecification, KategoriResponseDto, Long>
{
	
	private final KategoriRepository kategoriRepository;
	
	private final ModelMapper mapper;
	
	private final KategoriSpecification spec;

	public Page<KategoriResponseDto> getAll(String search, Integer page, Integer limit, List<String> sortBy, Boolean desc,
			Boolean isDeleted) {

		String defaultSort = "id|asc";
		if (sortBy != null) {
			sortBy.add(defaultSort);
		} else {
			sortBy = Arrays.asList(defaultSort, "id|asc");
		}
		
		isDeleted = (isDeleted != null) ? isDeleted : false;
		
		Pageable pageableRequest = this.defaultPage(search, page, limit, sortBy, desc);
		String status = "notdeleted";
		if (isDeleted == true) {
			status = "isdeleted";
		}
		
		Specification<Kategori> filterSpec = this.defaultSpec(search, status, spec);
		
		Page<Kategori> kategoriPage = kategoriRepository.findAll(filterSpec, pageableRequest);
		List<Kategori> kategoriList = kategoriPage.getContent();
		List<KategoriResponseDto> responseDto = kategoriList.stream().map(kategori -> mapper.map(kategori, KategoriResponseDto.class))
				.collect(Collectors.toList());
		return new PageImpl<>(responseDto, pageableRequest, kategoriPage.getTotalElements());
	}
}
