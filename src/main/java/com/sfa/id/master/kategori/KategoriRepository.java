package com.sfa.id.master.kategori;

import org.springframework.stereotype.Repository;

import com.sfa.id.core.base.BaseRepository;

@Repository
public interface KategoriRepository extends BaseRepository<Kategori, Long> {

}
