package com.sfa.id.master.kategori;

import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sfa.id.core._enum.ResponseCodeEnum;
import com.sfa.id.core.response.BaseResponse;
import com.sfa.id.master.kategori.dto.KategoriRequestDto;
import com.sfa.id.master.kategori.dto.KategoriResponseDto;
import com.sfa.id.master.kategori.service.KategoriQueryService;
import com.sfa.id.master.kategori.service.KategoriTransactionService;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/kategori")
@RequiredArgsConstructor
public class KategoriController {
	
	private final KategoriTransactionService transactionService;
	
	private final KategoriQueryService queryService;
	
	private final ModelMapper mapper;
	
	@GetMapping
	public ResponseEntity<?> getAll(
			@RequestParam(value = "search", required = false) String search,
			@RequestParam(value = "page", required = false) Integer page,
			@RequestParam(value = "limit", required = false) Integer limit,
			@RequestParam(value = "sortBy", required = false) List<String> sortBy,
			@RequestParam(value = "descending", required = false) Boolean desc,
			@RequestParam(value = "isDeleted", required = false) Boolean isDeleted
			) {
		BaseResponse<Page<KategoriResponseDto>> response = new BaseResponse<>();
		Page<KategoriResponseDto> data = queryService.getAll(search, page, limit, sortBy, desc, isDeleted);
		
		response.setErrorMessages(null);
		response.setStatusCode(ResponseCodeEnum.SUCCESS);
		response.setData(data);
		
		return ResponseEntity.ok(response);
	}
	
	@PostMapping
	public ResponseEntity<?> store(@Valid @RequestBody KategoriRequestDto requestDto) {
		BaseResponse<KategoriResponseDto> response = new BaseResponse<>();
		
		if (!transactionService.doSave(response, requestDto)) {
			response.setData(null);
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
		}
		
		KategoriResponseDto responseDto = mapper.map(requestDto, KategoriResponseDto.class);
		
		response.setStatusCode(ResponseCodeEnum.SAVE_SUCCESS.getCode());
		response.setMessage(ResponseCodeEnum.SAVE_SUCCESS.getDescription());
		response.setErrorMessages(null);
		response.setData(responseDto);
		
		return ResponseEntity.ok(response);
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<?> update(@Valid @RequestBody KategoriRequestDto request, @PathVariable("id") Long id) {
		BaseResponse<KategoriResponseDto> response = new BaseResponse<>();
		request.setId(id);
		
		if (!transactionService.doUpdate(response, request)) {
			response.setData(null);
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
		}
		
		KategoriResponseDto responseDto = mapper.map(request, KategoriResponseDto.class);
		
		response.setStatusCode(ResponseCodeEnum.UPDATE_SUCCESS.getCode());
		response.setMessage(ResponseCodeEnum.UPDATE_SUCCESS.getDescription());
		response.setErrorMessages(null);
		response.setData(responseDto);
		
		
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}

}
