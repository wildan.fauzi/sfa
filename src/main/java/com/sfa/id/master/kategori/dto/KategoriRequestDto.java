package com.sfa.id.master.kategori.dto;


import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

import com.sfa.id.core.regex.RegexStringPattern;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class KategoriRequestDto {
	private Long id;
	
	@NotBlank
	@Pattern(regexp = RegexStringPattern.ALPHANUMERIC_WITH_SPACES_ONLY)
	private String namaKategori;
	

}
