package com.sfa.id.helper;

import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import com.sfa.id.auth.jabatan.Jabatan;
import com.sfa.id.auth.jabatan.JabatanRepository;
import com.sfa.id.auth.role.Role;
import com.sfa.id.auth.role.RoleRepository;
import com.sfa.id.auth.user.UserRepository;
import com.sfa.id.auth.user.dto.UserRequestDto;
import com.sfa.id.auth.user.service.UserTransactionService;
import com.sfa.id.core.response.BaseResponse;
import com.sfa.id.master.kategori.Kategori;
import com.sfa.id.master.kategori.KategoriRepository;

import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
public class DatabaseSeeder {

	private final RoleRepository roleRepository;
	
	private final UserRepository userRepository;
	
	private final JabatanRepository jabatanRepository;
	
	private final KategoriRepository kategoriRepository;
	
	private final UserTransactionService userService;
	
	
	@EventListener
	public void DatabaseSeeder(ApplicationReadyEvent event) {
		this.RoleSeeder();
		this.JabatanSeeder();
		this.UserSeeder();
		this.KategoriSeeder();
	}
	
	private void RoleSeeder() {
		if (roleRepository.count() == 0) {
			roleRepository.save(new Role(null, "Admin"));
			roleRepository.save(new Role(null, "Sales Person"));
			roleRepository.save(new Role(null, "Sales Manager"));
			roleRepository.save(new Role(null, "Management"));
		}
	}
	
	private void JabatanSeeder() {
		if (jabatanRepository.count() == 0) {
			jabatanRepository.save(new Jabatan(null, "Management"));
		}
	}
	
	private void UserSeeder() {
		if (userRepository.count() == 0) {
			BaseResponse response = new BaseResponse<>();
			UserRequestDto request = new UserRequestDto();
			request.setNik("3210013223333");
			request.setEmail("admin@gmail.com");
			request.setNamaDepan("John");
			request.setNamaBelakang("Olshon");
			request.setPassword("123456");
			request.setPhone("081222333444");
			request.setGender("LK");
			request.setJabatanId(1L);
			request.setRoleId(1L);
			userService.doSave(response, request);
			
			request.setNik("321001251019999001");
			request.setEmail("Elise@gmail.com");
			request.setNamaDepan("Elise");
			request.setNamaBelakang("Beverley");
			request.setPassword("123456");
			request.setPhone("082123777654");
			request.setGender("PR");
			request.setJabatanId(1L);
			request.setRoleId(3L);
			userService.doSave(response, request);
			
			request.setNik("321001101020000001");
			request.setEmail("izumi@gmail.com");
			request.setNamaDepan("Izumi");
			request.setNamaBelakang("Katsuyoshi");
			request.setPassword("123456");
			request.setPhone("085155007767");
			request.setGender("LK");
			request.setJabatanId(1L);
			request.setRoleId(2L);
			userService.doSave(response, request);
			
		}
	}
	
	private void KategoriSeeder() {
		if (kategoriRepository.count() == 0) {
			kategoriRepository.save(new Kategori(null, "Kartu Perdana"));
			kategoriRepository.save(new Kategori(null, "Voucher Paket Data"));
			kategoriRepository.save(new Kategori(null, "Voucher Pulsa"));
			kategoriRepository.save(new Kategori(null, "Merchandise"));
			kategoriRepository.save(new Kategori(null, "Voucher Roaming"));
			
		}
	}
	
}
