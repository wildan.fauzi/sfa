package com.sfa.id.core.constant;

public class PageConstants {
	public static final Integer PAGE = 0;
	public static final Integer LIMIT= 5;
	public static final Boolean DESC = false;
	public static final String SEARCH = "";
}
