package com.sfa.id.core.security;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;

import com.fasterxml.jackson.databind.ObjectMapper;

public class CustomAccessDeniedHandler implements AccessDeniedHandler {

	@Override
	public void handle(HttpServletRequest request, HttpServletResponse response,
			AccessDeniedException accessDeniedException) throws IOException, ServletException {
		response.setContentType("application/json");
		response.setStatus(HttpServletResponse.SC_FORBIDDEN);
		
		Map<String, Object> resp = new LinkedHashMap<>();
		resp.put("timestamp", new Date().getTime());
		resp.put("status", HttpServletResponse.SC_FORBIDDEN);
		resp.put("error", "Forbidden");
		resp.put("message", "Forbidden - " + request.getServletPath());
		resp.put("path", request.getServletPath());
		
		ObjectMapper objectMapper = new ObjectMapper();
		PrintWriter out = response.getWriter();
        out.print(objectMapper.writeValueAsString(resp));
		out.close();
	}

}
