package com.sfa.id.core.regex;

public class RegexStringPattern {

	public static final String ALPHANUMERIC_ONLY = "^[a-zA-Z0-9]*$";
	public static final String ALPHANUMERIC_WITH_SPACES_ONLY = "^[a-zA-Z0-9_ ]*$";
	public static final String NUMERIC_ONLY = "^$|^[0-9]*$";
	public static final String DATE_FORMAT_ONLY = "^([0-2][0-9]|(3)[0-1])(\\/)(((0)[0-9])|((1)[0-2]))(\\/)\\d{4}$";
	public static final String TIME_FORMAT_ONLY = "^([0-1]?[0-9]|2[0-3]):[0-5][0-9]$";
	public static final String DECIMAL_WITH_OR_WITHOUT_DOT = "^$|^\\d*\\.?\\d+$";
	public static final String ALPHANUMERIC_WITH_SYMBOL = "^[a-zA-Z0-9 ~!@#$%^&*()_`]*$";
	public static final String COORDINATE = "^[-|0-9]{1}[0-9.]*$";
	public static final String ALPHANUMERIC_WITH_UNDERSCORE = "^[a-zA-Z0-9_]*$";
	public static final String ALPHANUMERIC_WITH_SYMBOL_WITHOUT_SPACE = "^[a-zA-Z0-9~!@#$%^&*()_`]*$";
	public static final String DATETIME_FORMAT = "^(([012][0-9])|(3[01]))\\/([0][1-9]|1[012])\\/\\d\\d\\d\\d (20|21|22|23|[0-1]?\\d):[0-5]?\\d:[0-5]?\\d$";
	public static final String EMAIL_FORMAT = "^[\\w-\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
}
