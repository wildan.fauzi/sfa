package com.sfa.id.core.advice;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.ConversionNotSupportedException;
import org.springframework.beans.TypeMismatchException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.CannotCreateTransactionException;
import org.springframework.validation.BindException;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingPathVariableException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.context.request.async.AsyncRequestTimeoutException;
import org.springframework.web.multipart.support.MissingServletRequestPartException;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.sfa.id.core._enum.ResponseCodeEnum;
import com.sfa.id.core.exception.BusinessException;
import com.sfa.id.core.response.BaseError;
import com.sfa.id.core.response.BaseResponse;
import com.sfa.id.core.response.BaseResponseUtil;
import com.sfa.id.core.response.util.MessageSourceUtil;


@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice(annotations = {RestController.class, Service.class})
public class ApiExceptionHandlerAdvice extends ResponseEntityExceptionHandler {

	private final Logger LOGGER = LoggerFactory.getLogger(getClass());
	
	@Autowired
	
    private MessageSourceUtil messageSourceUtil;
	
	@ExceptionHandler(value = BusinessException.class)
    @ResponseBody
    public ResponseEntity<?> accessDeniedException(BusinessException ex, WebRequest request) {
    	LOGGER.error("accessDeniedException: {}", ex);
        return ResponseEntity.status(HttpStatus.FORBIDDEN).body(ex.getBaseResponseMessage());
    }
	
//	@ExceptionHandler(value = BaseDataIntegrityViolationException.class)
//    @ResponseBody
//    public ResponseEntity<?> dataIntegrityViolationException(BaseDataIntegrityViolationException ex, WebRequest request) {
//    	LOGGER.error("dataIntegrityViolationException: {}", ex);
//        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ex.getBaseResponseMessage());
//    }
	
	@ExceptionHandler(value = Exception.class)
    @ResponseBody
    public ResponseEntity<?> exception(Exception ex, WebRequest request) {
    	LOGGER.error("Exception Handle Advice: {}", ex);
        BaseResponse baseResponseMessage = new BaseResponse();
        baseResponseMessage.setStatusCode(ResponseCodeEnum.FAILED);
        baseResponseMessage.getErrorMessages().add(BaseError.builder().field(messageSourceUtil.getMessageSource("global.field.internalservererror")).message("Terjadi error, harap hubungi IT. Error Hint : Exception Handle Advice").build());
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(baseResponseMessage);
    }
	
	@Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
    	LOGGER.error("handleMethodArgumentNotValid: {}", ex);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                BaseResponseUtil.formatErrors(ex.getBindingResult().getFieldErrors()));
    }
	
	@Override
    protected ResponseEntity<Object> handleBindException(BindException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
    	LOGGER.error("handleBindException: {}", ex);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(BaseResponseUtil.formatErrors(ex.getBindingResult().getFieldErrors()));
    }
	
	@Override
    protected ResponseEntity<Object> handleHttpMediaTypeNotSupported(HttpMediaTypeNotSupportedException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
    	LOGGER.error("handleHttpMediaTypeNotSupported: {}", ex);
        return ResponseEntity.status(HttpStatus.UNSUPPORTED_MEDIA_TYPE).body(BaseResponseUtil.formatException(ResponseCodeEnum.INVALID_PARAMETER, "Media Type Not Supported / Invalid format form input"));
    }
	
	@Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        LOGGER.error("handleHttpMessageNotReadable: {}", ex);
        BaseResponse baseResponseMessage = new BaseResponse();
        baseResponseMessage.setStatusCode(ResponseCodeEnum.FAILED);
        baseResponseMessage.getErrorMessages().add(BaseError.builder().field(messageSourceUtil.getMessageSource("global.field.requestbody")).message("Terjadi error, harap hubungi IT. Error Hint : handleHttpMessageNotReadable").build());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(baseResponseMessage);
    }
	
	@ExceptionHandler(value = { RuntimeException.class})
    protected ResponseEntity<Object> handle(RuntimeException ex, HttpServletRequest request, HttpServletResponse response) {
        LOGGER.error("Exception Handle RuntimeException: {}", ex);
        BaseResponse baseResponseMessage = new BaseResponse();
        baseResponseMessage.setStatusCode(ResponseCodeEnum.FAILED);
        baseResponseMessage.getErrorMessages().add(BaseError.builder().field("BAD_REQUEST").message("Runtime Error : " + ex.getMessage()).build());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(baseResponseMessage);
    }
	
	 @ExceptionHandler(value = { CannotCreateTransactionException.class})
	    protected ResponseEntity<Object> handle(CannotCreateTransactionException ex, HttpServletRequest request, HttpServletResponse response) {
	        LOGGER.error("Exception CannotCreateTransactionException: {}", ex);
	        BaseResponse baseResponseMessage = new BaseResponse();
	        baseResponseMessage.setStatusCode(ResponseCodeEnum.FAILED);
	        baseResponseMessage.getErrorMessages().add(BaseError.builder().field("BAD_REQUEST").message("Cannot Create Transaction").build());
	        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(baseResponseMessage);
	    }

	    @ExceptionHandler(value = { IOException.class})
	    protected ResponseEntity<Object> handle(IOException ex, HttpServletRequest request, HttpServletResponse response) {
	        LOGGER.error("Exception IOException: {}", ex);
	        BaseResponse baseResponseMessage = new BaseResponse();
	        baseResponseMessage.setStatusCode(ResponseCodeEnum.FAILED);
	        baseResponseMessage.getErrorMessages().add(BaseError.builder().field("BAD_REQUEST").message("Internal Server Error").build());
	        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(baseResponseMessage);
	    }

	    @Override
	    protected ResponseEntity<Object> handleHttpRequestMethodNotSupported(HttpRequestMethodNotSupportedException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
	        LOGGER.error("Exception handleHttpRequestMethodNotSupported: {}", ex);
	        BaseResponse baseResponseMessage = new BaseResponse();
	        baseResponseMessage.setStatusCode(ResponseCodeEnum.FAILED);
	        baseResponseMessage.getErrorMessages().add(BaseError.builder().field("METHOD_FAILURE").message("Method Not Supported").build());
	        return ResponseEntity.status(HttpStatus.METHOD_FAILURE).body(baseResponseMessage);
	    }

	    @Override
	    protected ResponseEntity<Object> handleHttpMediaTypeNotAcceptable(HttpMediaTypeNotAcceptableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
	        LOGGER.error("Exception handleHttpMediaTypeNotAcceptable: {}", ex);
	        BaseResponse baseResponseMessage = new BaseResponse();
	        baseResponseMessage.setStatusCode(ResponseCodeEnum.FAILED);
	        baseResponseMessage.getErrorMessages().add(BaseError.builder().field("UNSUPPORTED_MEDIA_TYPE").message("Media Type Not Acceptable").build());
	        return ResponseEntity.status(HttpStatus.UNSUPPORTED_MEDIA_TYPE).body(baseResponseMessage);
	    }

	    @Override
	    protected ResponseEntity<Object> handleMissingPathVariable(MissingPathVariableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
	        LOGGER.error("Exception handleMissingPathVariable: {}", ex);
	        BaseResponse baseResponseMessage = new BaseResponse();
	        baseResponseMessage.setStatusCode(ResponseCodeEnum.FAILED);
	        baseResponseMessage.getErrorMessages().add(BaseError.builder().field("UNPROCESSABLE_ENTITY").message("Missing Path Variable").build());
	        return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(baseResponseMessage);
	    }

	    @Override
	    protected ResponseEntity<Object> handleMissingServletRequestParameter(MissingServletRequestParameterException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
	        LOGGER.error("Exception handleMissingServletRequestParameter: {}", ex);
	        BaseResponse baseResponseMessage = new BaseResponse();
	        baseResponseMessage.setStatusCode(ResponseCodeEnum.FAILED);
	        baseResponseMessage.getErrorMessages().add(BaseError.builder().field("BAD_REQUEST").message("Missing Servlet Request Parameter").build());
	        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(baseResponseMessage);
	    }

	    @Override
	    protected ResponseEntity<Object> handleServletRequestBindingException(ServletRequestBindingException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
	        LOGGER.error("Exception handleServletRequestBindingException: {}", ex);
	        BaseResponse baseResponseMessage = new BaseResponse();
	        baseResponseMessage.setStatusCode(ResponseCodeEnum.FAILED);
	        baseResponseMessage.getErrorMessages().add(BaseError.builder().field("BAD_REQUEST").message("Servlet Request Binding Exception").build());
	        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(baseResponseMessage);
	    }

	    @Override
	    protected ResponseEntity<Object> handleConversionNotSupported(ConversionNotSupportedException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
	        LOGGER.error("Exception handleConversionNotSupported: {}", ex);
	        BaseResponse baseResponseMessage = new BaseResponse();
	        baseResponseMessage.setStatusCode(ResponseCodeEnum.FAILED);
	        baseResponseMessage.getErrorMessages().add(BaseError.builder().field("BAD_REQUEST").message("Conversion Not Supported").build());
	        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(baseResponseMessage);
	    }

	    @Override
	    protected ResponseEntity<Object> handleTypeMismatch(TypeMismatchException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
	        LOGGER.error("Exception handleTypeMismatch: {}", ex);
	        BaseResponse baseResponseMessage = new BaseResponse();
	        baseResponseMessage.setStatusCode(ResponseCodeEnum.FAILED);
	        baseResponseMessage.getErrorMessages().add(BaseError.builder().field("BAD_REQUEST").message("Type Mismatch").build());
	        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(baseResponseMessage);
	    }

	    @Override
	    protected ResponseEntity<Object> handleHttpMessageNotWritable(HttpMessageNotWritableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
	        LOGGER.error("Exception handleHttpMessageNotWritable: {}", ex);
	        BaseResponse baseResponseMessage = new BaseResponse();
	        baseResponseMessage.setStatusCode(ResponseCodeEnum.FAILED);
	        baseResponseMessage.getErrorMessages().add(BaseError.builder().field("BAD_REQUEST").message("Message Not Writable").build());
	        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(baseResponseMessage);
	    }

	    @Override
	    protected ResponseEntity<Object> handleMissingServletRequestPart(MissingServletRequestPartException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
	        LOGGER.error("Exception handleMissingServletRequestPart: {}", ex);
	        BaseResponse baseResponseMessage = new BaseResponse();
	        baseResponseMessage.setStatusCode(ResponseCodeEnum.FAILED);
	        baseResponseMessage.getErrorMessages().add(BaseError.builder().field("BAD_REQUEST").message("Missing Servlet Request Part").build());
	        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(baseResponseMessage);
	    }

	    @Override
	    protected ResponseEntity<Object> handleNoHandlerFoundException(NoHandlerFoundException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
	        LOGGER.error("Exception handleNoHandlerFoundException: {}", ex);
	        BaseResponse baseResponseMessage = new BaseResponse();
	        baseResponseMessage.setStatusCode(ResponseCodeEnum.FAILED);
	        baseResponseMessage.getErrorMessages().add(BaseError.builder().field("BAD_REQUEST").message("No Handler Found").build());
	        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(baseResponseMessage);
	    }

	    @Override
	    protected ResponseEntity<Object> handleAsyncRequestTimeoutException(AsyncRequestTimeoutException ex, HttpHeaders headers, HttpStatus status, WebRequest webRequest) {
	        LOGGER.error("Exception handleAsyncRequestTimeoutException: {}", ex);
	        BaseResponse baseResponseMessage = new BaseResponse();
	        baseResponseMessage.setStatusCode(ResponseCodeEnum.FAILED);
	        baseResponseMessage.getErrorMessages().add(BaseError.builder().field("REQUEST_TIMEOUT").message("Async Request Timeout").build());
	        return ResponseEntity.status(HttpStatus.REQUEST_TIMEOUT).body(baseResponseMessage);
	    }

	    @Override
	    protected ResponseEntity<Object> handleExceptionInternal(Exception ex, Object body, HttpHeaders headers, HttpStatus status, WebRequest request) {
	        LOGGER.error("Exception handleExceptionInternal: {}", ex);
	        BaseResponse baseResponseMessage = new BaseResponse();
	        baseResponseMessage.setStatusCode(ResponseCodeEnum.FAILED);
	        baseResponseMessage.getErrorMessages().add(BaseError.builder().field("REQUEST_TIMEOUT").message("Exception").build());
	        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(baseResponseMessage);
	    }
}
