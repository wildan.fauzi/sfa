package com.sfa.id.core.base;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface BaseRepository<T, ID> extends JpaRepository<T, ID>, JpaSpecificationExecutor {

	//deletion_flagr
    @Query("select e from #{#entityName} e where e.deletionFlag=false")
    public List<T> findAllNotDeleted();

    //Look up deleted entities
    @Query("select e from #{#entityName} e where e.deletionFlag=true")
    public List<T> findAllDeleted();
    
    // Soft Delete
    @Modifying
    default public void softDelete(T obj) {
        this.save(obj);
    }

    @Modifying
    default public void restoreDelete(T obj) {
        this.save(obj);
    }
}
