package com.sfa.id.core.base;

import com.sfa.id.core._enum.ResponseCodeEnum;
import com.sfa.id.core.response.BaseResponse;

public interface QueryServiceInterface<T, B> {
	
	default public boolean beforeView(BaseResponse baseResponse, T request){
        return true;
    }
    default public boolean view(BaseResponse baseResponse, T request) {
        return true;
    }
    default public boolean afterView(BaseResponse baseResponse, T request){
        return true;
    }
    
    default public boolean doView(BaseResponse baseResponse, T request) {
        if (beforeView(baseResponse, request)) {
            if (view(baseResponse, request)) {
                if (afterView(baseResponse, request)) {
                    return true;
                } else {
                    baseResponse.setStatusCode(ResponseCodeEnum.AFTER_VIEW_FAILED);
                    return false;
                }
            } else {
                baseResponse.setStatusCode(ResponseCodeEnum.VIEW_FAILED);
                return false;
            }
        } else {
            baseResponse.setStatusCode(ResponseCodeEnum.BEFORE_VIEW_FAILED);
            return false;
        }
    }

}
