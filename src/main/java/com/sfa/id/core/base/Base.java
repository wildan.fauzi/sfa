package com.sfa.id.core.base;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Transient;

import lombok.Getter;
import lombok.Setter;

@MappedSuperclass
@Getter
@Setter
public abstract class Base implements Cloneable{

	
	@Column(name = "created_by", length = 100)
    private String createdBy;

    @Column(name = "created_dt")
    private Date createdDt;

    @Column(name = "last_updated_by", length = 100)
    private String lastUpdatedBy;

    @Column(name = "last_updated_dt")
    private Date lastUpdatedDt;

    @Column(name = "deleted_by", length = 100)
    private String deletedBy;

    @Column(name = "deleted_dt")
    private Date deletedDt;

    @Column(name = "deletion_flag")
    private Boolean deletionFlag;

    @Column(name = "version")
    private Integer version;
    
    @Transient
    private Base base;
    
    public void mapAudit(Base base) {
    	if(base != null) {
    		this.setCreatedBy(base.getCreatedBy());
            this.setCreatedDt(base.getCreatedDt());

            this.setLastUpdatedBy(base.getLastUpdatedBy());
            this.setLastUpdatedDt(base.getLastUpdatedDt());

            this.setDeletionFlag(base.getDeletionFlag());
            this.setDeletedBy(base.getDeletedBy());
            this.setDeletedDt(base.getDeletedDt());
            this.setVersion(base.getVersion());

            this.setBase(base);
    	}
    }

    public Base createInstance(){
        return this;
    }

    public Base clone() throws CloneNotSupportedException {
        return (Base) super.clone();
    }
	
}
