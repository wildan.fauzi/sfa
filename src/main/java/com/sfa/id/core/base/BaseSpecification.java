package com.sfa.id.core.base;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

public abstract class BaseSpecification<T> {
	private Boolean caseSensitive = false;
	
	public Specification<T> containsTextAllInOmni(String text) {

        if (!text.contains("%")) {
            text = "%" + text + "%";
        }
        final String finalText = caseSensitive ? text : text.toLowerCase();

        return new Specification<T>() {
            @Override
            public Predicate toPredicate(Root<T> root, CriteriaQuery<?> cq, CriteriaBuilder builder) {
                return builder.or(root.getModel().getDeclaredSingularAttributes().stream().filter(a -> {
                            if (a.getJavaType().getSimpleName().equalsIgnoreCase("string")) {
                                return true;
                            } else {
                                return false;
                            }
                        }).map(a -> builder.like(caseSensitive ? root.get(a.getName()) : builder.lower(root.get(a.getName())), finalText)
                        ).toArray(Predicate[]::new)
                );
            }
        };
    }
	
	public Specification<T> containsTextInAttributes(String text, List<String> attributes) {
        if (!text.contains("%")) {
            text = "%" + text + "%";
        }
        String finalText = caseSensitive ? text : text.toLowerCase();
        return (root, query, builder) -> builder.or(root.getModel().getDeclaredSingularAttributes().stream()
                .filter(a -> attributes.contains(a.getName()))
                .map(a -> builder.like(caseSensitive ? root.get(a.getName()) : builder.lower(root.get(a.getName())), finalText))
                .toArray(Predicate[]::new)
        );
    }

    public Specification<T> isActive() {
        return (root, query, builder) -> builder.equal(root.get("active"), Boolean.TRUE);
    }

    public Specification<T> isInActive() {
        return (root, query, builder) -> builder.equal(root.get("active"), Boolean.FALSE);
    }

    public Specification<T> notDeleted() {
        return (root, query, builder) -> builder.equal(root.get("deletionFlag"), Boolean.FALSE);
    }

    public Specification<T> isDeleted() {
        return (root, query, builder) -> builder.equal(root.get("deletionFlag"), Boolean.TRUE);
    }

    public abstract Specification<T> containsTextInOmni(String text);
    
    
}
