package com.sfa.id.core.base;

import com.sfa.id.core._enum.ResponseCodeEnum;
import com.sfa.id.core.response.BaseResponse;

// TODO: change name to TransactionServiceInterface
public interface TransactionServiceInterface<T, B> {

	default public boolean beforeSave(BaseResponse baseResponse, T request){
        return true;
    }
    default public boolean save(BaseResponse baseResponse, T request){
        return true;
    }
    default public boolean afterSave(BaseResponse baseResponse, T request){
        return true;
    }

    default public boolean beforeUpdate(BaseResponse baseResponse, T request){
        return true;
    }
    default public boolean update(BaseResponse baseResponse, T request){
        return true;
    }
    default public boolean afterUpdate(BaseResponse baseResponse, T request){
        return true;
    }

    default public boolean beforeDelete(BaseResponse baseResponse, T request){
        return true;
    }
    default public boolean delete(BaseResponse baseResponse, T request){
        return true;
    }
    default public boolean afterDelete(BaseResponse baseResponse, T request){
        return true;
    }

    default public boolean beforeRestore(BaseResponse baseResponse, T request){
    	return true; 
    }
    
    default public boolean restore(BaseResponse baseResponse, T request){ 
    	return true; 
    }
    default public boolean afterRestore(BaseResponse baseResponse, T request){ 
    	return true; 
    }
    
    default public boolean doSave(BaseResponse baseResponse, T request) {
        if (beforeSave(baseResponse, request)) {
            if (save(baseResponse, request)) {
                if (afterSave(baseResponse, request)) {
                    return true;
                } else {
                    baseResponse.setStatusCode(ResponseCodeEnum.AFTER_SAVE_FAILED);
                    return false;
                }
            } else {
                baseResponse.setStatusCode(ResponseCodeEnum.SAVE_FAILED);
                return false;
            }
        } else {
            baseResponse.setStatusCode(ResponseCodeEnum.BEFORE_SAVE_FAILED);
            return false;
        }
    }

    default public boolean doUpdate(BaseResponse baseResponse, T request) {
        if (beforeUpdate(baseResponse, request)) {
            if (update(baseResponse, request)) {
                if (afterUpdate(baseResponse, request)) {
                    return true;
                } else {
                    baseResponse.setStatusCode(ResponseCodeEnum.AFTER_UPDATE_FAILED);
                    return false;
                }
            } else {
                baseResponse.setStatusCode(ResponseCodeEnum.UPDATE_FAILED);
                return false;
            }
        } else {
            baseResponse.setStatusCode(ResponseCodeEnum.BEFORE_UPDATE_FAILED);
            return false;
        }
    }

    default public boolean doDelete(BaseResponse baseResponse, T request) {
        if (beforeDelete(baseResponse, request)) {
            if (delete(baseResponse, request)) {
                if (afterDelete(baseResponse, request)) {
                    return true;
                } else {
                    baseResponse.setStatusCode(ResponseCodeEnum.AFTER_DELETE_FAILED);
                    return false;
                }
            } else {
                baseResponse.setStatusCode(ResponseCodeEnum.DELETE_FAILED);
                return false;
            }
        } else {
            baseResponse.setStatusCode(ResponseCodeEnum.BEFORE_DELETE_FAILED);
            return false;
        }
    }

    default public boolean doRestore(BaseResponse baseResponse, T request) {
        if (beforeRestore(baseResponse, request)) {
            if (restore(baseResponse, request)) {
                if (afterRestore(baseResponse, request)) {
                    return true;
                } else {
                    baseResponse.setStatusCode(ResponseCodeEnum.AFTER_RESTORE_FAILED);
                    return false;
                }
            } else {
                baseResponse.setStatusCode(ResponseCodeEnum.RESTORE_FAILED);
                return false;
            }
        } else {
            baseResponse.setStatusCode(ResponseCodeEnum.BEFORE_RESTORE_FAILED);
            return false;
        }
    }
	
}
