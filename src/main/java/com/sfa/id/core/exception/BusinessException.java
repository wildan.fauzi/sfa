package com.sfa.id.core.exception;

import com.sfa.id.core.response.BaseResponse;

import lombok.Getter;
import lombok.Setter;

// TODO rename to BusinessException
@Getter @Setter
public class BusinessException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    private BaseResponse baseResponseMessage;

    public BusinessException(BaseResponse baseResponseMessage, String message) {
        super(message);
        this.baseResponseMessage = baseResponseMessage;
    }

    public BusinessException(String message) {
        super(message);
    }

}
