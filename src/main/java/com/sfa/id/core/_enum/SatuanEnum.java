package com.sfa.id.core._enum;

import lombok.Getter;

@Getter
public enum SatuanEnum {
	
	BOX("box"),
	PCS("pcs");
	
	private String name;
	
	private SatuanEnum(String name) {
		this.name = name;
	}
	
}
