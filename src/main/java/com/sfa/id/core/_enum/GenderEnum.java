package com.sfa.id.core._enum;

import lombok.Getter;

@Getter
public enum GenderEnum {
	LK("Laki-Laki"),
	PR("Perempuan");
	
	private String name;
	
	private GenderEnum(String name) {
		this.name = name;
	}
}
