package com.sfa.id.core._enum;

import lombok.Getter;

@Getter
public enum KategoriPajakEnum {

	PKP("Pengusaha Kena Pajak"),
	NONPKP("Non-PKP");
	
	private String name;
	
	private KategoriPajakEnum(String name) {
		this.name = name;
	}
}
