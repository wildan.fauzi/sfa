package com.sfa.id.core._enum;

import lombok.Getter;

@Getter
public enum ReferensiHargaEnum {
	LUARKOTA("Harga Luar Kota"),
	DALAMKOTA("Harga Dalam Kota");
	
	private String name;
	
	private ReferensiHargaEnum(String name) {
		this.name = name;
	}
}
