package com.sfa.id.core._enum;

import lombok.Getter;

@Getter
public enum ResponseCodeEnum {

    SUCCESS("1000", "Proses berhasil"),
    INVALID_PARAMETER("1001", "Parameter tidak sesuai"),
    UNAUTHORIZED_PROCESS("1002", "Tidak ada otorisasi terhadap proses tersebut"),
    FAILED("1003", "Proses gagal"),

    VIEW_SUCCESS("2000", "Proses view berhasil"),
    BEFORE_VIEW_FAILED("2001", "Proses view gagal, tidak lewat validasi"),
    VIEW_FAILED("2002", "Proses view gagal, sesuatu terjadi, cek log aplikasi"),
    AFTER_VIEW_FAILED("2003", "Proses view gagal, tidak bisa mengembalikan data"),

    SAVE_SUCCESS("3000", "Proses save berhasil"),
    BEFORE_SAVE_FAILED("3001", "Proses save gagal, tidak lewat validasi"),
    SAVE_FAILED("3002", "Proses save gagal, sesuatu terjadi, cek log aplikasi"),
    AFTER_SAVE_FAILED("3003", "Proses save gagal, tidak bisa mengembalikan data"),

    UPDATE_SUCCESS("4000", "Proses update berhasil"),
    BEFORE_UPDATE_FAILED("4001", "Proses update gagal, tidak lewat validasi"),
    UPDATE_FAILED("4002", "Proses update gagal, sesuatu terjadi, cek log aplikasi"),
    AFTER_UPDATE_FAILED("4003", "Proses update gagal, tidak bisa mengembalikan data"),

    DELETE_SUCCESS("5000", "Proses delete berhasil"),
    BEFORE_DELETE_FAILED("5001", "Proses delete gagal, tidak lewat validasi"),
    DELETE_FAILED("5002", "Proses delete gagal, sesuatu terjadi, cek log aplikasi"),
    AFTER_DELETE_FAILED("5003", "Proses delete gagal, tidak bisa mengembalikan data"),

    RESTORE_SUCCESS("6000", "Proses restore berhasil"),
    BEFORE_RESTORE_FAILED("6001", "Proses restore gagal, tidak lewat validasi"),
    RESTORE_FAILED("6002", "Proses restore gagal, sesuatu terjadi, cek log aplikasi"),
    AFTER_RESTORE_FAILED("6003", "Proses restore gagal, tidak bisa mengembalikan data"),

    INVALID_STATUS_TRANSACTION("7001", "Status transaksi tidak mendukung proses ini"),
    INVALID_REFUND_AMOUNT("7002", "Nominal pengembalian dana tidak sesuai"),
    INVALID_HISTORY_TRANSACTION("7003", "Terdapat kesalahan pada histori pembelian");

    private String code;

    private String description;

    private ResponseCodeEnum(String code, String description) {
        this.code = code;
        this.description = description;
    }
}
