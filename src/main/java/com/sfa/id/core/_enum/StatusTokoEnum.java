package com.sfa.id.core._enum;

import lombok.Getter;

@Getter
public enum StatusTokoEnum {
	NORMAL("Normal"),
	PERINGANTAN("Peringatan");
	
	private String name;
	
	private StatusTokoEnum(String name) {
		this.name = name;
	}
}	
