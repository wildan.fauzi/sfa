package com.sfa.id.core.response.util;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

@Component
public class MessageSourceUtil {

	@Autowired
	private MessageSource messageSource;

	public String getMessageSource(String message) {
		Locale locale = LocaleContextHolder.getLocale();
		String result = messageSource.getMessage(message, null, locale).toString();
		return result;
	}

	public String getMessageSourceWithParam(String message, String param) {
		Locale locale = LocaleContextHolder.getLocale();
		String result = messageSource.getMessage(message,  new Object[]{param}, locale).toString();
		return result;
	}
	
	public String getMessageSourceWithMultipleParam(String message,Object[] param) {
		Locale locale = LocaleContextHolder.getLocale();
		String result = messageSource.getMessage(message, param, locale).toString();
		return result;
	}
}
