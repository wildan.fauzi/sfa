package com.sfa.id.core.response;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sfa.id.core._enum.ResponseCodeEnum;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@AllArgsConstructor
@Getter @Setter
public class BaseResponse<T> {

	private String statusCode;
    private String message;
    private List<BaseError> errorMessages;
    private T data;
    @JsonIgnore
    private Optional<?> base;
    
    public BaseResponse() {
        this.errorMessages = new ArrayList<>();
    }
    
    public void setStatusCode(String statusCode){
        this.statusCode = statusCode;
    }
    
    //[INFO]:
    //fungsi ini akan dipakai ketika ingin mengembalikkan pesan general
    //detail pesan error akan dipasang di list error messages
    //kalau ingin memberi pesan general yg custom harus mengset statusCode dan message lagi
    public void setStatusCode(ResponseCodeEnum responseCodeEnum) {
        this.statusCode = responseCodeEnum.getCode();
        this.message = responseCodeEnum.getDescription();
    }
}
