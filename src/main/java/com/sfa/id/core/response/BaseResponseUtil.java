package com.sfa.id.core.response;

import org.springframework.validation.FieldError;

import com.sfa.id.core._enum.ResponseCodeEnum;

import java.util.ArrayList;
import java.util.List;

public class BaseResponseUtil {

    public static BaseResponse<?> returnSuccessResponse() {
        return BaseResponse.builder()
                .statusCode(ResponseCodeEnum.SUCCESS.getCode())
                .message(ResponseCodeEnum.SUCCESS.toString())
                .data(null)
                .build();
    }

    public static BaseResponse<?> formatObject(Object object) {
        return BaseResponse.builder()
                .statusCode(ResponseCodeEnum.SUCCESS.getCode())
                .message(ResponseCodeEnum.SUCCESS.toString())
                .data(object)
                .build();
    }

    public static BaseResponse<?> formatException(ResponseCodeEnum status, String exceptionMessage) {
        List<BaseError> errors = new ArrayList<>();
        errors.add(BaseError.builder().field("Xooply").message(exceptionMessage).build());

        return BaseResponse.builder()
                .statusCode(status.getCode())
                .message(status.getDescription())
                .errorMessages(errors)
                .build();
    }

    public static BaseResponse<?> formatErrors(List<FieldError> errors) {
        List<BaseError> respErrors = new ArrayList<>();
        for(FieldError error:errors) {
            respErrors.add(BaseError.builder()
                    .field(error.getField())
                    .message(error.getDefaultMessage())
                    .build());
        }

        return BaseResponse.builder()
                .statusCode(ResponseCodeEnum.INVALID_PARAMETER.getCode())
                .message(ResponseCodeEnum.INVALID_PARAMETER.getDescription())
                .errorMessages(respErrors)
                .build();
    }
}
