package com.sfa.id.core.utils;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class GeneralUtils {

	@Bean
	public ModelMapper modelMapper() {
		return new ModelMapper();
	}
}
