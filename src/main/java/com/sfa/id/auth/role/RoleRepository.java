package com.sfa.id.auth.role;

import org.springframework.stereotype.Repository;

import com.sfa.id.core.base.BaseRepository;

@Repository
public interface RoleRepository extends BaseRepository<Role, Long>{

}
