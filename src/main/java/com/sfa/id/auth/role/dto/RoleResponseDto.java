package com.sfa.id.auth.role.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class RoleResponseDto {

	private Long id;
	private String name;
}
