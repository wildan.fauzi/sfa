package com.sfa.id.auth.user;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sfa.id.auth.user.dto.UserRequestDto;
import com.sfa.id.auth.user.service.UserTransactionService;
import com.sfa.id.core._enum.ResponseCodeEnum;
import com.sfa.id.core.response.BaseResponse;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/users")
@RequiredArgsConstructor
public class UserController {

	private final UserTransactionService transactionService;
	
	@PostMapping("/register")
	public ResponseEntity<?> register(@RequestBody UserRequestDto request) {
		BaseResponse<User> response = new BaseResponse<>();
		
		if (!transactionService.doSave(response, request)) {
			response.setData(null);
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
		}
		
		response.setStatusCode(ResponseCodeEnum.SAVE_SUCCESS.getCode());
		response.setMessage(ResponseCodeEnum.SAVE_SUCCESS.getDescription());
		response.setErrorMessages(null);
		
		return ResponseEntity.ok(response);
	}
	
}
