package com.sfa.id.auth.user.dto;

import javax.validation.constraints.NotBlank;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class UserRequestDto {

	private Long id;
	
	@NotBlank
	private String nik;
	
	@NotBlank
	private String email;
	
	@NotBlank
	private String namaDepan;
	
	@NotBlank
	private String namaBelakang;
	
	@NotBlank
	private String password;
	
	@NotBlank
	private String phone;
	
	@NotBlank
	private String gender;
	
	private String image;
	
	@NotBlank
	private Long jabatanId;
	
	@NotBlank
	private Long roleId;
}

