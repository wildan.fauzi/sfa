package com.sfa.id.auth.user;

import java.util.Optional;

import org.springframework.stereotype.Repository;

import com.sfa.id.core.base.BaseRepository;

@Repository
public interface UserRepository extends BaseRepository<User, Long>{
	
	Optional<User> findByEmail(String email);
}
