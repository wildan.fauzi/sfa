package com.sfa.id.auth.user.dto;

import com.sfa.id.auth.jabatan.dto.JabatanResponseDto;
import com.sfa.id.auth.role.dto.RoleResponseDto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class UserSummaryResponseDto {
	
	private Long id;
	private String namaLengkap;
	private String namaBelakang;
	private RoleResponseDto role;
	private JabatanResponseDto jabatan;

}
