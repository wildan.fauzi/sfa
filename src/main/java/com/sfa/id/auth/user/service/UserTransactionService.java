package com.sfa.id.auth.user.service;

import org.modelmapper.ModelMapper;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.sfa.id.auth.role.Role;
import com.sfa.id.auth.role.RoleRepository;
import com.sfa.id.auth.user.User;
import com.sfa.id.auth.user.UserRepository;
import com.sfa.id.auth.user.dto.UserRequestDto;
import com.sfa.id.core._enum.GenderEnum;
import com.sfa.id.core._enum.SatuanEnum;
import com.sfa.id.core.base.TransactionServiceInterface;
import com.sfa.id.core.response.BaseError;
import com.sfa.id.core.response.BaseResponse;
import com.sfa.id.master.produk.dto.ProdukRequestDto;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class UserTransactionService implements TransactionServiceInterface<UserRequestDto, User>, UserDetailsService{

	private final UserRepository userRepository;
	
	private final RoleRepository roleRepository;
	
	private final ModelMapper mapper;
	
	private final BCryptPasswordEncoder bCryptPasswordEncoder;
	
	@Override
	public boolean beforeSave(BaseResponse baseResponse, UserRequestDto request) {
		return checkExist(baseResponse, request) && checkRole(baseResponse, request) && checkGender(baseResponse, request);
	}
	
	@Override
	public boolean save(BaseResponse baseResponse, UserRequestDto request) {
		User user = mapper.map(request, User.class);
		String encodePassword = bCryptPasswordEncoder.encode(user.getPassword());
		user.setPassword(encodePassword);
		
		try {
			userRepository.save(user);
			return true;
		} catch (Exception e) {
			baseResponse.getErrorMessages()
			.add(BaseError.builder()
                    .field("User")
                    .message("Gagal menambahkan user")
                    .build());
			return false;
		}
	}

	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		return userRepository.findByEmail(email).orElseThrow(() -> 
		new UsernameNotFoundException(
				String.format("Can't find user with email %s", email))
		);
	}
	
	private boolean checkGender(BaseResponse baseResponse, UserRequestDto request) {
		
		String gender = request.getGender().toUpperCase();
		
		try {
			request.setGender(GenderEnum.valueOf(gender).getName());
			return true;
		} catch (Exception e) {
			baseResponse.getErrorMessages()
				.add(BaseError.builder()
                  .field("Gender")
                  .message("Gagal menambahkan User. Gender tidak terdaftar tidak terdaftar. LK = Laki-laki, PR =  Perempuan")
                  .build());
		}
		
		return false;
	}
	
	private boolean checkExist(BaseResponse response, UserRequestDto request) {
		User user = userRepository.findByEmail(request.getEmail()).orElse(null);
		
		if (user != null) {
			response.getErrorMessages()
			.add(BaseError.builder()
                    .field("User")
                    .message("Username sudah terdaftar")
                    .build());
			return false;
		}
		
		return true;
	}
	
	private boolean checkRole(BaseResponse response, UserRequestDto request) {
		Role role = roleRepository.findById(request.getRoleId()).orElse(null);
		
		
		if (role == null) {
			response.getErrorMessages()
			.add(BaseError.builder()
                    .field("User")
                    .message("Role tidak ditemukan")
                    .build());
			return false;
		}
		
		
		return true;
	}
	
	
}
