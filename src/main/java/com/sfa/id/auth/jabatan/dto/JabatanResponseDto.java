package com.sfa.id.auth.jabatan.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class JabatanResponseDto {

	private Long id;
	private String name;
}
