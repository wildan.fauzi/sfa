package com.sfa.id.auth.jabatan;

import org.springframework.stereotype.Repository;

import com.sfa.id.core.base.BaseRepository;

@Repository
public interface JabatanRepository extends BaseRepository<Jabatan, Long>{

}
