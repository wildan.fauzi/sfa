CREATE TABLE IF NOT EXISTS m_alamat_pelanggan
(
    id bigserial not null,
    nama_kantor varchar(255),
    no_telepon varchar(16),
    alamat varchar(255),
    pelanggan_id int,
    
    
    created_by varchar(20),
    created_dt timestamp,
    last_updated_by varchar(20),
    last_updated_dt timestamp,
    deleted_by varchar(20),
    deleted_dt timestamp,
    deletion_flag boolean default false,
    version int,
    primary key (id),
    
    CONSTRAINT fk_pelanggan_id FOREIGN KEY (pelanggan_id) 
    REFERENCES m_pelanggan(id) ON DELETE CASCADE
    )
;