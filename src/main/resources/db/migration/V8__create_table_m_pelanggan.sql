CREATE TABLE IF NOT EXISTS m_pelanggan
(
    id bigserial not null,
    nama_toko varchar(255),
    nama_pemilik varchar(255),
    no_telepon varchar(16),
    email varchar(64),
    status varchar(32),
    deskripsi text,
    provinsi_id int,
    kota_id int,
    alamat varchar(255),
    nik varchar(32),
    image_nik varchar(255),
    npwp varchar(32),
    image_npwp varchar(255),
    kategori_pajak varchar(128),
    pic varchar(255),
    no_telepon_pic varchar(16),
    sales_manager_id int,
    sales_person_id int,
    referensi_harga varchar(64),
    diskon_reguler decimal(3,1),
    diskon_distributor decimal(3,1),
    
    
    created_by varchar(20),
    created_dt timestamp,
    last_updated_by varchar(20),
    last_updated_dt timestamp,
    deleted_by varchar(20),
    deleted_dt timestamp,
    deletion_flag boolean default false,
    version int,
    primary key (id),
    
    CONSTRAINT fk_provinsi_id FOREIGN KEY (provinsi_id) 
    REFERENCES m_provinsi(id) ON DELETE CASCADE,
    CONSTRAINT fk_kota_id FOREIGN KEY (kota_id) 
    REFERENCES m_kota(id) ON DELETE CASCADE,
    CONSTRAINT fk_sales_manager_id FOREIGN KEY (sales_manager_id) 
    REFERENCES s_users(id) ON DELETE CASCADE,
    CONSTRAINT fk_sales_person_id FOREIGN KEY (sales_person_id) 
    REFERENCES s_users(id) ON DELETE CASCADE
    )
;