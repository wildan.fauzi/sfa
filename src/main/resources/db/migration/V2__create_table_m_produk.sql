CREATE TABLE IF NOT EXISTS m_produk
(
    produk_id bigserial not null,
    kategori_id integer,
    sku varchar(20),
    nama_produk varchar(255),
    satuan varchar(8),
    harga decimal,
    deskripsi text,
    
    created_by varchar(20),
    created_dt timestamp,
    last_updated_by varchar(20),
    last_updated_dt timestamp,
    deleted_by varchar(20),
    deleted_dt timestamp,
    deletion_flag boolean default false,
    version int,
    PRIMARY KEY (produk_id),
    CONSTRAINT fk_kategori_id FOREIGN KEY (kategori_id) 
    REFERENCES m_kategori(kategori_id) ON DELETE CASCADE
    )
;