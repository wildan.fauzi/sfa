CREATE TABLE IF NOT EXISTS m_kota
(
	id bigserial not null,
  	provinsi_id int,
    nama varchar(255),
    
    PRIMARY KEY (id),
    CONSTRAINT fk_provinsi_id FOREIGN KEY (provinsi_id) 
    REFERENCES m_provinsi(id) ON DELETE CASCADE
    )
;