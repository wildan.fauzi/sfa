CREATE TABLE IF NOT EXISTS s_jabatan	
(
	id bigserial not null,
	name varchar(255),
    
     created_by varchar(20),
    created_dt timestamp,
    last_updated_by varchar(20),
    last_updated_dt timestamp,
    deleted_by varchar(20),
    deleted_dt timestamp,
    deletion_flag boolean default false,
    version int,
    PRIMARY KEY (id)
    )
;