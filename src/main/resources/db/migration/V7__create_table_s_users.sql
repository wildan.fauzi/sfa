CREATE TABLE IF NOT EXISTS s_users
(
	id bigserial not null,
	nik varchar(32),
	email varchar(255),
	nama_depan varchar(255),
	nama_belakang varchar(255),
	password varchar(255),
	phone varchar(255),
	gender varchar(32),
	image varchar(255),
	
	jabatan_id int,
	role_id int,
    
    created_by varchar(20),
    created_dt timestamp,
    last_updated_by varchar(20),
    last_updated_dt timestamp,
    deleted_by varchar(20),
    deleted_dt timestamp,
    deletion_flag boolean default false,
    version int,
    PRIMARY KEY (id),
    CONSTRAINT fk_role_id FOREIGN KEY (role_id) 
    REFERENCES s_roles(id) ON DELETE CASCADE,
    CONSTRAINT fk_jabatan_id FOREIGN KEY (jabatan_id) 
    REFERENCES s_jabatan(id) ON DELETE CASCADE
    )
;